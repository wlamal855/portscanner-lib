# portscanner-lib
This software provides simple TCP and UDP port scanner functionality. It can be used to implement a full fledged port scanner or to create server discovery capabilities. The library is written in Java.

*IPv6 should work, but has not been tested in a production environment!*


# License
    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at
	
        http://www.apache.org/licenses/LICENSE-2.0
	
    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.