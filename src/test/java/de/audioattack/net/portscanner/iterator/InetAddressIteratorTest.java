package de.audioattack.net.portscanner.iterator;

import static org.junit.Assert.assertEquals;

import java.math.BigInteger;
import java.net.Inet4Address;
import java.net.Inet6Address;
import java.net.UnknownHostException;

import org.junit.Test;

public class InetAddressIteratorTest {

  final private InetAddressIterator oneBlockInet4AddressIterator;
  final private InetAddressIterator oneBlockInet6AddressIterator;

  private final BigInteger ONE_BLOCK_SIZE = BigInteger.valueOf(256);

  public InetAddressIteratorTest() throws UnknownHostException {

    oneBlockInet4AddressIterator = new InetAddressIterator(Inet4Address.getByName("localhost"), 24,
        true);
    oneBlockInet6AddressIterator = new InetAddressIterator(
        Inet6Address.getByName("0:0:0:0:0:0:0:1"), 120, true);
  }

  @Test
  public void testNext() {

    assertEquals(oneBlockInet4AddressIterator.getApproxCount(),
        getTrueCount(oneBlockInet4AddressIterator));
    assertEquals(oneBlockInet6AddressIterator.getApproxCount(),
        getTrueCount(oneBlockInet6AddressIterator));
  }

  private BigInteger getTrueCount(final InetAddressIterator inetAddressIterator) {

    BigInteger iter = BigInteger.ZERO;
    while (inetAddressIterator.hasNext()) {
      inetAddressIterator.next();
      iter = iter.add(BigInteger.ONE);
    }
    return iter;
  }

  @Test
  public void testGetApproxCount() {

    assertEquals(ONE_BLOCK_SIZE, oneBlockInet4AddressIterator.getApproxCount());
    assertEquals(ONE_BLOCK_SIZE, oneBlockInet6AddressIterator.getApproxCount());
  }

}
