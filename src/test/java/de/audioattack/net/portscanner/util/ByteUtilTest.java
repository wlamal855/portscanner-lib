package de.audioattack.net.portscanner.util;

import static org.junit.Assert.assertTrue;

import java.util.Arrays;

import org.junit.Test;

public class ByteUtilTest {

  @Test
  public final void testCreateBitMaskLeadingBitsSet() {
    assertTrue(Arrays.equals(ByteUtil.createBitMaskLeadingBitsSet((byte) 4, (byte) 32),
        new byte[] { (byte) 0xFF, (byte) 0xFF, (byte) 0xFF, (byte) 0xFF }));
    assertTrue(Arrays.equals(ByteUtil.createBitMaskLeadingBitsSet((byte) 4, (byte) 31),
        new byte[] { (byte) 0xFF, (byte) 0xFF, (byte) 0xFF, (byte) 0xFE }));
    assertTrue(Arrays.equals(ByteUtil.createBitMaskLeadingBitsSet((byte) 4, (byte) 24),
        new byte[] { (byte) 0xFF, (byte) 0xFF, (byte) 0xFF, 0x00 }));
    assertTrue(Arrays.equals(ByteUtil.createBitMaskLeadingBitsSet((byte) 4, (byte) 16),
        new byte[] { (byte) 0xFF, (byte) 0xFF, 0x00, 0x00 }));
    assertTrue(Arrays.equals(ByteUtil.createBitMaskLeadingBitsSet((byte) 4, (byte) 8),
        new byte[] { (byte) 0xFF, 0x00, 0x00, 0x00 }));
    assertTrue(Arrays.equals(ByteUtil.createBitMaskLeadingBitsSet((byte) 4, (byte) 0),
        new byte[] { 0x00, 0x00, 0x00, 0x00 }));
  }

}
