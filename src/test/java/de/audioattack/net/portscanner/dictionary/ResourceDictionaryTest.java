package de.audioattack.net.portscanner.dictionary;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.Arrays;

import org.junit.Before;
import org.junit.Test;

import de.audioattack.net.portscanner.dictionary.tcp.IanaTcpDictionary;
import de.audioattack.net.portscanner.dictionary.tcp.NmapTcpDictionary;

public class ResourceDictionaryTest {

  private ResourceDictionary resourceDictionary;
  private IanaTcpDictionary ianaDictionary;
  private NmapTcpDictionary nmapDictionary;

  @Before
  public void setUp() {

    try {
      resourceDictionary = new ResourceDictionary("test.txt");
    } catch (IllegalArgumentException e) {
      fail(e.getMessage());
    }

    ianaDictionary = new IanaTcpDictionary();
    nmapDictionary = new NmapTcpDictionary();
  }

  @Test
  public void portTest() {

    final int[] allPorts = resourceDictionary.getAllPorts();

    assertTrue(allPorts.length == 5);

    Arrays.sort(allPorts);

    for (final int port : new int[] { 80, 443, 12345, 665 }) {
      assertTrue(Arrays.binarySearch(allPorts, port) > -1);
    }
  }

  @Test
  public void protocolInfoTest() {

    assertNotEquals("anything", ianaDictionary.getProtocolInfo(80).get(0).getName());
    assertEquals("http", ianaDictionary.getProtocolInfo(80).get(0).getName());
    assertEquals("https", ianaDictionary.getProtocolInfo(443).get(0).getName());

    assertNotEquals("anything", nmapDictionary.getProtocolInfo(80).get(0).getName());
    assertEquals("http", nmapDictionary.getProtocolInfo(80).get(0).getName());
    assertEquals("https", nmapDictionary.getProtocolInfo(443).get(0).getName());

    assertEquals("http", resourceDictionary.getProtocolInfo(80).get(0).getName());
    assertEquals("https", resourceDictionary.getProtocolInfo(443).get(0).getName());
    assertEquals("idk", resourceDictionary.getProtocolInfo(12345).get(0).getName());
    assertEquals("abc", resourceDictionary.getProtocolInfo(665).get(0).getName());
    assertEquals("example", resourceDictionary.getProtocolInfo(80).get(0).getDescription());
    assertEquals("https example", resourceDictionary.getProtocolInfo(443).get(0).getDescription());
    assertEquals("idk example", resourceDictionary.getProtocolInfo(12345).get(0).getDescription());
    assertEquals("who", resourceDictionary.getProtocolInfo(1234).get(0).getName());
    assertEquals("cares", resourceDictionary.getProtocolInfo(1234).get(1).getName());
    assertTrue(resourceDictionary.getProtocolInfo(665).get(0).getDescription().isEmpty());
    assertNull(resourceDictionary.getProtocolInfo(667));
    assertNull(resourceDictionary.getProtocolInfo(668));
  }

}
