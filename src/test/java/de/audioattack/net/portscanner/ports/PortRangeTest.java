package de.audioattack.net.portscanner.ports;

import static org.junit.Assert.assertTrue;

import org.junit.Test;

import de.audioattack.net.portscanner.iterator.PortRange;

public class PortRangeTest {

  @Test
  public void test() {

    PortRange portRange = new PortRange(0, 0);

    assertTrue(portRange.size() == 1);

    portRange = new PortRange(1, 100);

    assertTrue(portRange.size() == 100);

  }

}
