package de.audioattack.net.portscanner.ports;

import static org.junit.Assert.assertTrue;

import org.junit.Test;

import de.audioattack.net.portscanner.iterator.SinglePortIterator;

public class SinglePortIteratorTest {

  @Test
  public void test() {

    int[] ports = new int[] { 0, 1, 2 };
    SinglePortIterator singlePortIterator = new SinglePortIterator(0, 1, 2);

    assertTrue(singlePortIterator.size() == ports.length);

    int[] array = new int[singlePortIterator.size()];
    for (int i = 0; singlePortIterator.hasNext(); i++) {

      array[i] = singlePortIterator.next();
    }

  }

}
