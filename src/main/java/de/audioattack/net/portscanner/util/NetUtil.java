// Copyright 2013 - 2015 Marc Nause <marc.nause@gmx.de>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package de.audioattack.net.portscanner.util;

import java.net.Inet4Address;
import java.net.Inet6Address;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.util.Collection;
import java.util.Enumeration;
import java.util.HashSet;

/**
 * Contains utility methods for network information.
 * 
 * @since 1.0.0
 */
public class NetUtil {

  /**
   * Length of IPv4 netmask in byte.
   */
  private static final int IP4_NETMASK_LENGTH = 4;

  /**
   * Length of IPv6 netmask in byte.
   */
  private static final int IP6_NETMASK_LENGTH = 16;

  /** Protected constructor to avoid instantiation of static utility class. */
  protected NetUtil() {
  }

  /**
   * Creates a netmask from a given prefix length which is valid for IPv4.
   * <br />
   * <br />
   * Examples:
   * <ul>
   * <li>24 -> 255.255.255.0</li>
   * <li>12 -> 255.255.0.0</li>
   * <li>8 -> 255.0.0.0</li>
   * </ul>
   * 
   * @param networkPrefixLength
   *          prefix length to create netmask from.
   * @return the netmask
   */
  public static byte[] createInet4Netmask(final int networkPrefixLength) {

    if (!isValidInet4NetworkPrefixLength(networkPrefixLength)) {
      throw new IllegalArgumentException(
          "Prefix length out of bounds [0, 32]: " + networkPrefixLength);
    }

    return ByteUtil.createBitMaskLeadingBitsSet(IP4_NETMASK_LENGTH, networkPrefixLength);
  }

  /**
   * Creates a netmask from a given prefix length which is valid for IPv6.
   * 
   * @param networkPrefixLength
   *          prefix length to create netmask from.
   * @return the netmask
   * 
   * @see #createInet4Netmask(int)
   */
  public static byte[] createInet6Netmask(final int networkPrefixLength) {

    if (!isValidInet6NetworkPrefixLength(networkPrefixLength)) {
      throw new IllegalArgumentException(
          "Prefix length out of bounds [0, 128]: " + networkPrefixLength);
    }

    return ByteUtil.createBitMaskLeadingBitsSet(IP6_NETMASK_LENGTH, networkPrefixLength);
  }

  /**
   * Gets public network addresses of device code is running on.
   * 
   * @return contains all public addresses which could be found
   * @throws SocketException
   *           if an i/O error occurs
   */
  public static Collection<InetAddress> getPublicAddresses() throws SocketException {

    final Collection<InetAddress> inetAddresses = new HashSet<InetAddress>();

    final Enumeration<NetworkInterface> interfaces = NetworkInterface.getNetworkInterfaces();
    while (interfaces.hasMoreElements()) {
      final NetworkInterface iface = interfaces.nextElement();
      final Enumeration<InetAddress> addresses = iface.getInetAddresses();

      while (addresses.hasMoreElements()) {
        final InetAddress addr = addresses.nextElement();
        if (!addr.isLoopbackAddress()) {
          inetAddresses.add(addr);
        }
      }
    }

    return inetAddresses;
  }

  /**
   * Creates netmask for given address (required to determine if IPv4 or IPv6 is
   * used) and network prefix length.
   * 
   * @param inetAddress
   *          valid IPv4 or IPv6 address
   * @param networkPrefixLength
   *          prefix length which matches IP version of given address
   * @return the netmask
   */
  public static byte[] getNetmask(final InetAddress inetAddress, final int networkPrefixLength) {

    final byte[] netmask;
    if (inetAddress instanceof Inet4Address) {

      netmask = NetUtil.createInet4Netmask(networkPrefixLength);
    } else if (inetAddress instanceof Inet6Address) {

      netmask = NetUtil.createInet6Netmask(networkPrefixLength);
    } else {

      throw new IllegalArgumentException(
          "Unknown InetAddress implementation: " + inetAddress.getClass().getName());
    }

    return netmask;
  }

  /**
   * Tells if a network prefix length is valid for IP version of a given
   * InetAddress.
   * 
   * @param inetAddress
   *          valid IPv4 or IPv6 address
   * @param networkPrefixLength
   *          prefix length to check
   * @return {@code true} if network prefix length is valid, else {@code false}
   */
  public static boolean isValidNetmaskPrefixLength(final InetAddress inetAddress,
      final int networkPrefixLength) {

    return (inetAddress instanceof Inet4Address
        && isValidInet4NetworkPrefixLength(networkPrefixLength))
        || (inetAddress instanceof Inet6Address
            && isValidInet6NetworkPrefixLength(networkPrefixLength));
  }

  /**
   * Tells if a network prefix length is valid for IPv4.
   * 
   * @param networkPrefixLength
   *          prefix length to check
   * @return {@code true} if network prefix length is valid for IPv4, else
   *         {@code false}
   */
  public static boolean isValidInet4NetworkPrefixLength(final int networkPrefixLength) {

    return networkPrefixLength >= 0 && networkPrefixLength <= 32;
  }

  /**
   * Tells if a network prefix length is valid for IPv6.
   * 
   * @param networkPrefixLength
   *          prefix length to check
   * @return {@code true} if network prefix length is valid for IPv6, else
   *         {@code false}
   */
  public static boolean isValidInet6NetworkPrefixLength(final int networkPrefixLength) {

    return networkPrefixLength >= 0 && networkPrefixLength <= 128;
  }

}
