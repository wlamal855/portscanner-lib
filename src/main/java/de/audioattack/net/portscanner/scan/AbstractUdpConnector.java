package de.audioattack.net.portscanner.scan;

import java.io.IOException;
import java.lang.ref.WeakReference;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetSocketAddress;
import java.net.PortUnreachableException;
import java.net.SocketTimeoutException;
import java.util.UUID;

import de.audioattack.net.portscanner.listener.IScannerStatusListener;
import de.audioattack.net.portscanner.scan.enums.PortResult;
import de.audioattack.net.portscanner.scan.enums.TransportProtocol;

/**
 * Base class for UDP connectors.
 * 
 * @since 1.0.0
 */
abstract class AbstractUdpConnector extends AbstractScanConnector {

  /**
   * Constructor.
   * 
   * @param jobId
   *          ID of scan job
   * @param inetSocketAddress
   *          address of host to scan
   * @param timeout
   *          max. time to wait for socket connection
   * @param statusListenerReference
   *          callback to report result to
   */
  AbstractUdpConnector(final UUID jobId, final InetSocketAddress inetSocketAddress,
      final int timeout, WeakReference<IScannerStatusListener> statusListenerReference) {
    super(jobId, inetSocketAddress, timeout, statusListenerReference);
  }

  @Override
  protected PortResult connect() {

    final DatagramPacket packet = new DatagramPacket(new byte[128], 128);
    DatagramSocket toSocket = null;
    {
      int retry = 0;
      while (retry++ < 3) {
        try {

          toSocket = new DatagramSocket();
          toSocket.connect(getInetSocketAddress());
          toSocket.setSoTimeout(getTimeout());
          toSocket.send(packet);
          toSocket.receive(packet);

          return PortResult.OPEN;
        } catch (PortUnreachableException e) {
          return PortResult.CLOSED;
        } catch (SocketTimeoutException e) {
          // ignore silently, we'll try again
        } catch (IOException e) {
          return PortResult.CLOSED;
        } finally {
          if (toSocket != null) {
            toSocket.close();
          }
        }
      }
    }

    return PortResult.OPEN_FILTERED;
  }

  @Override
  protected TransportProtocol getTransportProtocol() {
    return TransportProtocol.UDP;
  }

}
