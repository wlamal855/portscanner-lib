package de.audioattack.net.portscanner.scan;

import java.io.IOException;
import java.lang.ref.WeakReference;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.util.UUID;

import de.audioattack.net.portscanner.listener.IScannerStatusListener;
import de.audioattack.net.portscanner.scan.enums.PortResult;
import de.audioattack.net.portscanner.scan.enums.TransportProtocol;

/**
 * Base class for TCP connectors.
 * 
 * @since 1.0.0
 */
abstract class AbstractTcpConnector extends AbstractScanConnector {

  /**
   * Constructor.
   * 
   * @param jobId
   *          ID of scan job
   * @param inetSocketAddress
   *          address of host to scan
   * @param timeout
   *          max. time to wait for socket connection
   * @param statusListenerReference
   *          callback to report result to
   */
  AbstractTcpConnector(final UUID jobId, final InetSocketAddress inetSocketAddress,
      final int timeout, WeakReference<IScannerStatusListener> statusListenerReference) {
    super(jobId, inetSocketAddress, timeout, statusListenerReference);
  }

  @Override
  protected PortResult connect() {

    PortResult ret;
    try {
      final Socket socket = new Socket();
      socket.connect(getInetSocketAddress(), getTimeout());
      socket.close();
      ret = PortResult.OPEN;
    } catch (final IOException e) {
      ret = PortResult.CLOSED;
    }
    return ret;
  }

  @Override
  protected TransportProtocol getTransportProtocol() {
    return TransportProtocol.TCP;
  }

}
