// Copyright 2016 Marc Nause <marc.nause@gmx.de>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package de.audioattack.net.portscanner.iterator;

import java.util.Iterator;

/**
 * Interface for iterator which iterates over several ports.
 * 
 * @since 1.0.0
 */
public interface IPortIterator extends Iterator<Integer> {

  /**
   * Count of ports the iterator iterates over.
   * 
   * @return port count
   */
  int size();

}
