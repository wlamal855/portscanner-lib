// Copyright 2016 Marc Nause <marc.nause@gmx.de>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package de.audioattack.net.portscanner.iterator;

/**
 * Iterator which iterates over one or more {@link PortRange}s.
 * 
 * @since 1.0.0
 */
public class PortRangeIterator implements IPortIterator {

  private final PortRange[] myPortRanges;
  private final int mySize;
  private int myPortRangeIndex;
  private int myPortIndex;

  /**
   * Constructor.
   * 
   * @param thePortRanges
   *          port ranges to iterate over
   */
  public PortRangeIterator(final PortRange... thePortRanges) {

    if (thePortRanges.length == 0) {
      throw new IllegalArgumentException("Port range must not be empty.");
    }

    myPortRanges = thePortRanges;
    mySize = getSize(myPortRanges);
  }

  @Override
  public boolean hasNext() {

    return hasMorePorts() || hasMorePortRanges();
  }

  private boolean hasMorePorts() {

    return myPortRanges[myPortRangeIndex].getMyStart()
        + myPortIndex <= myPortRanges[myPortRangeIndex].getMyEnd();
  }

  private boolean hasMorePortRanges() {

    return myPortRangeIndex < myPortRanges.length - 1;
  }

  @Override
  public Integer next() {

    if (!hasMorePorts()) {
      myPortRangeIndex++;
      myPortIndex = 0;
    }

    return myPortRanges[myPortRangeIndex].getMyStart() + myPortIndex++;
  }

  private static int getSize(final PortRange[] thePortRanges) {

    int size = 0;
    for (final PortRange portRange : thePortRanges) {
      size += portRange.size();
    }

    return size;
  }

  @Override
  public int size() {
    return mySize;
  }

}
