// Copyright 2016 Marc Nause <marc.nause@gmx.de>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package de.audioattack.net.portscanner.iterator;

import java.util.Arrays;
import java.util.NoSuchElementException;

/**
 * Iterates over one or several ports.
 * 
 * @since 1.0.0
 */
public class SinglePortIterator implements IPortIterator {

  private final int[] myPorts;
  private int myIndex = 0;

  /**
   * Constructor.
   * 
   * @param port
   *          ports to iterate over
   */
  public SinglePortIterator(final int... port) {
    myPorts = Arrays.copyOf(port, port.length);
  }

  @Override
  public boolean hasNext() {

    return myIndex < size();
  }

  @Override
  public Integer next() {

    if (!hasNext()) {
      throw new NoSuchElementException();
    }

    return myPorts[myIndex++];
  }

  @Override
  public int size() {

    return myPorts.length;
  }

}
