// Copyright 2016 Marc Nause <marc.nause@gmx.de>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package de.audioattack.net.portscanner.dictionary.udp;

import de.audioattack.net.portscanner.dictionary.ResourceDictionary;

/**
 * UDP ports as known to Nmap <a href="https://nmap.org/">Nmap</a>.
 * 
 * @since 1.0.0
 */
public final class NmapUdpDictionary extends ResourceDictionary {

  /**
   * Constructor.
   * </p>
   * Please notice that constructing an instance of this class is expensive. You
   * may want to keep a reference to this class or wrap it into a singleton.
   */
  public NmapUdpDictionary() {
    super("nmap_udp_ports.txt");
  }
}
