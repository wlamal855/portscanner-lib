// Copyright 2016 Marc Nause <marc.nause@gmx.de>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package de.audioattack.net.portscanner.dictionary;

import java.util.List;
import java.util.Map;

/**
 * Base class for dictionaries which contain a port/protocol-mapping.
 * 
 * @since 1.0.0
 */
public abstract class AbstractDictionary implements IPortDictionary {

  private final boolean mayCache;

  private int[] ports;

  /**
   * Gets the port/protocol-mapping.
   * 
   * @return the mapping
   */
  protected abstract Map<Integer, List<ProtocolInfo>> getMapping();

  /**
   * Constructor.
   * 
   * @param mayCache
   *          {@code true} if mapping may be cached, {@code false} if mapping
   *          shall be fetched using {@link #getMapping()} every time
   *          {@link #getAllPorts()} is called.
   */
  public AbstractDictionary(final boolean mayCache) {
    this.mayCache = mayCache;
  }

  @Override
  public List<ProtocolInfo> getProtocolInfo(final int port) {

    return getMapping().get(port);
  }

  @Override
  public int[] getAllPorts() {

    int[] array = null;

    if (!mayCache || ports == null) {
      array = new int[getMapping().size()];

      int index = 0;
      for (final int port : getMapping().keySet()) {
        array[index++] = port;
      }

      if (mayCache) {
        ports = array;
      }
    }

    return mayCache ? ports : array;
  }

}
