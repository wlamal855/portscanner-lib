// Copyright 2013 - 2016 Marc Nause <marc.nause@gmx.de>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package de.audioattack.net.portscanner.dictionary.tcp;

import de.audioattack.net.portscanner.dictionary.ResourceDictionary;

/**
 * Standardized TCP ports as listed on the website of the
 * <a href="http://www.iana.org/assignments/service-names-port-numbers/">Service
 * Name and Transport Protocol Port Number Registry</a>.
 * 
 * @since 1.0.0
 */
public class IanaTcpDictionary extends ResourceDictionary {

  /**
   * Constructor.
   * </p>
   * Please notice that constructing an instance of this class is expensive. You
   * may want to keep a reference to this class or wrap it into a singleton.
   */
  public IanaTcpDictionary() {
    super("iana_tcp_ports.txt");
  }

}
