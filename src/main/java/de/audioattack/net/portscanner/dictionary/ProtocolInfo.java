// Copyright 2015 Marc Nause <marc.nause@gmx.de>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package de.audioattack.net.portscanner.dictionary;

/**
 * Simple POJO to keep protocol information.
 * 
 * @since 1.0.0
 */
public class ProtocolInfo {

  private final String myDescription;
  private final String myName;

  /**
   * Constructor.
   * 
   * @param theName
   *          name of protocol
   * @param theDescription
   *          description of protocol, may be {@code null} or empty
   */
  public ProtocolInfo(final String theName, final String theDescription) {
    myDescription = theDescription;
    myName = theName;
  }

  /**
   * Gets short description of protocol.
   * 
   * @return description of protocol, may be {@code null} or empty
   */
  public String getDescription() {
    return myDescription;
  }

  /**
   * Gets name of protocol.
   * 
   * @return name of protocol
   */
  public String getName() {
    return myName;
  }

}
