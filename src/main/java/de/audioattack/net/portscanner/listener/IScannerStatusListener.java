// Copyright 2013 - 2015 Marc Nause <marc.nause@gmx.de>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package de.audioattack.net.portscanner.listener;

import java.util.UUID;

import de.audioattack.net.portscanner.scan.enums.PortResult;
import de.audioattack.net.portscanner.scan.enums.TransportProtocol;

/**
 * Listener which will be informed about status changes of port scanner. Method
 * calls may be executed by different Threads. If values are added to
 * Collections, thread-safe variants should be chosen.
 * 
 * @since 1.0.0
 */
public interface IScannerStatusListener {

  /**
   * Called when scan is started.
   * 
   * @param jobId
   *          unique ID of scan job
   */
  void onScanStarted(UUID jobId);

  /**
   * Called when scan is finished.
   * 
   * @param jobId
   *          unique ID of scan job which has finished
   */
  void onScanFinished(UUID jobId);

  /**
   * Called when scan is aborted.
   * 
   * @param jobId
   *          unique ID of scan job which has been aborted
   */
  void onScanAborted(UUID jobId);

  /**
   * Called if a new host is found via ping.
   *
   * @param jobId
   *          unique ID of scan job which has found the host
   * @param host
   *          address of host
   */
  void onHostFound(UUID jobId, String host);

  /**
   * Called if a new port is found.
   *
   * @param jobId
   *          unique ID of scan job which has found the port
   * @param host
   *          address of host
   * @param port
   *          port number
   * @param result
   *          info about success or failure to connect to port
   * @param protocol
   *          protocol used when connecting
   */
  void onPortResult(UUID jobId, String host, int port, PortResult result,
      TransportProtocol protocol);

}
