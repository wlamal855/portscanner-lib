// Copyright 2016 Marc Nause <marc.nause@gmx.de>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package de.audioattack.net.portscanner.listener;

import java.util.UUID;

/**
 * Listener which will be informed about progress of scan job.
 * 
 * @since 1.0.0
 */
public interface IProgressListener {

  /**
   * Called when scan job or part of scan job is finished.
   * 
   * @param jobId
   *          unique ID of scan job
   * @param progress
   *          percentage of finished jobs (from 0.0 to 1.0)
   */
  void onJobsFinishedProgress(UUID jobId, float progress);
}
