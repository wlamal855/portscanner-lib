// Copyright 2012 - 2016 Marc Nause <marc.nause@gmx.de>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package de.audioattack.net.portscanner;

import java.lang.ref.WeakReference;
import java.net.Inet4Address;
import java.net.Inet6Address;
import java.net.InetAddress;
import java.net.SocketException;
import java.util.Collection;
import java.util.Collections;
import java.util.Map;
import java.util.UUID;
import java.util.WeakHashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.RejectedExecutionException;
import java.util.concurrent.TimeUnit;

import de.audioattack.net.portscanner.iterator.IPortIterator;
import de.audioattack.net.portscanner.iterator.InetAddressIterator;
import de.audioattack.net.portscanner.iterator.NullPortIterator;
import de.audioattack.net.portscanner.listener.IProgressListener;
import de.audioattack.net.portscanner.listener.IScannerStatusListener;
import de.audioattack.net.portscanner.scan.LowPriorityThreadFactory;
import de.audioattack.net.portscanner.scan.ScanProgress;
import de.audioattack.net.portscanner.scan.ScanRunnable;
import de.audioattack.net.portscanner.scan.enums.TransportProtocol;
import de.audioattack.net.portscanner.util.NetUtil;

/**
 * A simple multi-threaded TCP/UDP port scanner.
 * 
 * @since 1.0.0
 */
public final class PortScanner {

  /** Default maximum number of threads which will be used to scan network. */
  private static final int THREADS = 20;

  /** Maximum number of threads which will be used to scan network. */
  private final int myMaxThreads;

  /**
   * Maximum time to wait for response (in ms). Can be set via
   * {@link #setConnectionTimeout(int)}.
   */
  private int myConnectionTimeout = 10000;

  /**
   * Maximum time to wait for response (in ms). Can be set via
   * {@link #setPingTimeout(int)}.
   */
  private int myPingTimeout = 500;

  /**
   * Listener to be notified on scan events.
   */
  private final WeakReference<IScannerStatusListener> myStatusListener;

  /**
   * Optional listener to be notified about scan progress.
   */
  private final WeakReference<IProgressListener> myProgressListener;

  /**
   * Contains executor services of currently running scans.
   */
  private final Map<UUID, ExecutorService> myExecutorMapping = Collections
      .synchronizedMap(new WeakHashMap<UUID, ExecutorService>());

  /**
   * Constructor.
   *
   * @param statusListener
   *          Listener to be notified on scan events, must not be {@code null}
   */
  public PortScanner(final IScannerStatusListener statusListener) {
    this(statusListener, null);
  }

  /**
   * Constructor.
   *
   * @param statusListener
   *          Listener to be notified on scan events, must not be {@code null}
   * @param progressListener
   *          optional listener to be notified about scan progress
   */
  public PortScanner(final IScannerStatusListener statusListener,
      final IProgressListener progressListener) {
    this(THREADS, statusListener, progressListener);
  }

  /**
   * Constructor.
   *
   * @param threads
   *          number of parallel scan tasks
   * @param statusListener
   *          Listener to be notified on scan events, must not be {@code null}
   */
  public PortScanner(final int threads, final IScannerStatusListener statusListener) {
    this(threads, statusListener, null);
  }

  /**
   * Constructor.
   *
   * @param threads
   *          number of parallel scan tasks
   * @param statusListener
   *          Listener to be notified on scan events, must not be {@code null}
   * @param progressListener
   *          optional listener to be notified about scan progress
   */
  public PortScanner(final int threads, final IScannerStatusListener statusListener,
      final IProgressListener progressListener) {

    if (statusListener == null) {
      throw new IllegalArgumentException(
          IScannerStatusListener.class.getSimpleName() + " may not be null.");
    }

    myMaxThreads = threads;
    myStatusListener = new WeakReference<IScannerStatusListener>(statusListener);
    myProgressListener = new WeakReference<IProgressListener>(progressListener);
  }

  /**
   * Sets maximum time in ms to wait for connection.
   * 
   * @param timeout
   *          time to wait, must be positive number
   */
  public void setConnectionTimeout(final int timeout) {

    if (timeout < 0) {
      throw new IllegalArgumentException("Timeout may not be negative: " + timeout);
    }

    myConnectionTimeout = timeout;
  }

  /**
   * Sets maximum time in ms to wait until ping fails.
   * 
   * @param timeout
   *          time to wait, must be positive number
   */
  public void setPingTimeout(final int timeout) {

    if (timeout < 0) {
      throw new IllegalArgumentException("Timeout may not be negative: " + timeout);
    }

    myPingTimeout = timeout;
  }

  /**
   * Gets maximum time in ms to wait for connection.
   * 
   * @return the maximum time
   */
  public int getConnectionTimeout() {
    return myConnectionTimeout;
  }

  /**
   * Gets maximum time in ms to wait until ping fails.
   * 
   * @return the maximum time
   */
  public int getPingTimeout() {
    return myPingTimeout;
  }

  /**
   * Scans several ports of a single host.
   *
   * @param jobId
   *          a unique job ID which may be defined by the user. Random ID will
   *          be created if value is {@code null}.
   * @param protocol
   *          transport protocol used for scan (UDP scan is much slower than TCP
   *          scan!)
   * @param inetAddress
   *          has to contain address which can be resolved to IPv4 address or
   *          IPv6 address
   * @param ports
   *          ports to scan
   */
  public void scan(final UUID jobId, final TransportProtocol protocol,
      final InetAddress inetAddress, final IPortIterator ports) {

    final int networkPrefixLength;
    if (inetAddress instanceof Inet4Address) {
      networkPrefixLength = 32;
    } else if (inetAddress instanceof Inet6Address) {
      networkPrefixLength = 128;
    } else {
      throw new IllegalArgumentException(
          "Unknown InetAddress implementation: " + inetAddress.getClass().getName());
    }

    scan(jobId, protocol, inetAddress, networkPrefixLength, true, true, true, false, ports);
  }

  /**
   * Scans several ports of several hosts.
   *
   * @param jobId
   *          a unique job ID which may be defined by the user. Random ID will
   *          be created if value is {@code null}.
   * @param protocol
   *          transport protocol used for scan
   * @param baseAddress
   *          has to contain address which can be resolved to IPv4 address or
   *          IPv6 address
   * @param networkPrefixLength
   *          determines number of hosts to be scanned
   * @param scanOnlyIfReachable
   *          if true, host will only be scanned if
   *          {@link InetAddress#isReachable(int)} returns true, else no check
   * @param scanLocalAddresses
   *          true if local machine shall be included in the scan, else false
   * @param scanMulticastAddresses
   *          true if multicast addresses shall be included in the scan, else
   *          false
   * @param scanWholeBlock
   *          true to scan whole block which contains baseAddress, false to
   *          start with baseAddress
   * @param ports
   *          ports to scan
   */
  public void scan(final UUID jobId, final TransportProtocol protocol,
      final InetAddress baseAddress, final int networkPrefixLength,
      final boolean scanOnlyIfReachable, final boolean scanLocalAddresses,
      final boolean scanMulticastAddresses, final boolean scanWholeBlock,
      final IPortIterator ports) {

    if (!NetUtil.isValidNetmaskPrefixLength(baseAddress, networkPrefixLength)) {
      throw new IllegalArgumentException("Invalid netmask.");
    }

    Collection<InetAddress> publicLocalAddresses;
    try {
      publicLocalAddresses = NetUtil.getPublicAddresses();
    } catch (final SocketException e1) {
      publicLocalAddresses = Collections.emptySet();
    }

    final InetAddressIterator inetAddressIterator = new InetAddressIterator(baseAddress,
        networkPrefixLength, scanWholeBlock);

    /*
     * Scanning UDP ports works better if not scanning parallel.
     */
    final int maxThreads = protocol == TransportProtocol.UDP ? 1 : myMaxThreads;

    final ExecutorService executor = Executors.newFixedThreadPool(maxThreads,
        new LowPriorityThreadFactory());

    final UUID uuid = jobId == null ? getJobId() : jobId;

    myExecutorMapping.put(uuid, executor);

    fireOnStarted(uuid);

    final ScanProgress progress = new ScanProgress(uuid, myProgressListener,
        inetAddressIterator.getApproxCount(), ports.size());

    while (inetAddressIterator.hasNext() && !executor.isShutdown()) {
      final InetAddress generatedInetAddress = inetAddressIterator.next();

      if (generatedInetAddress != null
          && (scanLocalAddresses || (!generatedInetAddress.isLoopbackAddress()
              && !publicLocalAddresses.contains(generatedInetAddress)))
          && (scanMulticastAddresses || !generatedInetAddress.isMulticastAddress())
          && !executor.isShutdown()) {

        try {
          executor.execute(new ScanRunnable(uuid, generatedInetAddress, protocol, myPingTimeout,
              myConnectionTimeout, scanOnlyIfReachable, ports, myStatusListener, progress, executor,
              !inetAddressIterator.hasNext()));

        } catch (RejectedExecutionException ex) {
          /*
           * May happen if executor has been shut down just before we try to
           * execute Runnable. We can ignore this Exception since we don't care
           * about the Runnable anymore.
           */
        }
      } else {
        progress.incrementProgressByPortCount();
      }
    }

    try {

      /* Executor is shutdown by last ScanRunnable. */

      executor.awaitTermination(Long.MAX_VALUE, TimeUnit.DAYS);

    } catch (final InterruptedException e) {
      Thread.currentThread().interrupt();
    }

    fireOnFinished(uuid);

  }

  private void fireOnFinished(final UUID uuid) {
    final IScannerStatusListener listener = myStatusListener.get();
    if (listener != null) {
      listener.onScanFinished(uuid);
    }
  }

  private void fireOnStarted(final UUID uuid) {
    final IScannerStatusListener listener = myStatusListener.get();
    if (listener != null) {
      listener.onScanStarted(uuid);
    }
  }

  /**
   * Tries to find hosts in a network block.
   *
   * @param jobId
   *          a unique job ID which may be defined by the user. Random ID will
   *          be created if value is {@code null}.
   * @param baseAddress
   *          has to contain address which can be resolved to IPv4 address or
   *          IPv6 address
   * @param networkPrefixLength
   *          determines number of hosts to be scanned
   * @param scanLocalAddresses
   *          true if local machine shall be included in the scan, else false
   * @param scanMulticastAddresses
   *          true if multicast addresses shall be included in the scan, else
   *          false
   * @param scanWholeBlock
   *          true to scan whole block which contains baseAddress, false to
   *          start with baseAddress
   */
  public void discover(final UUID jobId, final InetAddress baseAddress,
      final int networkPrefixLength, final boolean scanLocalAddresses,
      final boolean scanMulticastAddresses, final boolean scanWholeBlock) {

    scan(jobId, TransportProtocol.TCP, baseAddress, networkPrefixLength, true, scanLocalAddresses,
        scanMulticastAddresses, scanWholeBlock, NullPortIterator.getInstance());
  }

  /**
   * Aborts a job identified by its ID. The ID can be retrieved when the job is
   * started.
   * 
   * @param jobId
   *          the unique job ID
   * @return {@code true} if job is terminated, else (e.g. job does not exist)
   *         {@code false}
   */
  public boolean abort(final UUID jobId) {

    final boolean success;

    final ExecutorService executor = myExecutorMapping.get(jobId);

    if (null == executor) {
      success = false;
    } else {
      executor.shutdownNow();
      success = executor.isShutdown();

      final IScannerStatusListener listener = myStatusListener.get();
      if (listener != null) {
        listener.onScanAborted(jobId);
      }
    }

    return success;
  }

  /**
   * Gets unique ID for a scan job.
   * 
   * @return unique ID
   */
  public static UUID getJobId() {
    return UUID.randomUUID();
  }

}
