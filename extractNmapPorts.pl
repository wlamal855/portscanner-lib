#!/usr/bin/perl

# use like this:
#
# ./extractNmapPorts.pl tcp > src/main/resources/nmap_tcp_ports.txt
# ./extractNmapPorts.pl udp > src/main/resources/nmap_udp_ports.txt

use strict;
use warnings;
use LWP::Simple;
use Text::CSV;

my $protocol = $ARGV[0];

my $URL = 'https://raw.githubusercontent.com/nmap/nmap/master/nmap-services';

print "#\n";
print
"# ports from https://raw.githubusercontent.com/nmap/nmap/master/nmap-services\n";
print "#\n";

my $lastPort = -1;
my @elements;
my @lines = split( '\R', get($URL) );
for my $line (@lines) {
	@elements = split( '\t', $line );

	(
		      defined( $elements[0] )
		  and defined( $elements[1] )
		  and $elements[0] ne ''
		  and $elements[0] ne 'unknown'
		  and $elements[1] =~ /^[0-9]+\/$protocol$/
	) or next;

	$elements[1] =~ s/\/$protocol//;

	if ( defined $elements[3] ) {
		$elements[3] =~ s/^# //;
		print "$elements[1]\t$elements[0]\t$elements[3]\n";
	}
	else {
		print "$elements[1]\t$elements[0]\t\n";
	}
}
