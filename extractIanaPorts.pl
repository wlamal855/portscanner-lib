#!/usr/bin/perl

# use like this:
#
# ./extractIanaPorts.pl tcp > src/main/resources/iana_tcp_ports.txt
# ./extractIanaPorts.pl udp > src/main/resources/iana_udp_ports.txt

use strict;
use warnings;
use LWP::Simple;
use Text::CSV;

my $protocol = $ARGV[0];

my $URL =
'http://www.iana.org/assignments/service-names-port-numbers/service-names-port-numbers.csv';

print "#\n";
print
"# ports from http://www.iana.org/assignments/service-names-port-numbers/service-names-port-numbers.xhtml\n";
print "#\n";

my $lastPort = -1;
my @elements;
my @lines = split( '\R', get($URL) );
my $csv = Text::CSV->new();
for my $line (@lines) {
	$csv->parse($line);
	@elements = $csv->fields();

	(
		      defined( $elements[0] )
		  and defined( $elements[1] )
		  and $elements[0] ne ''
		  and $elements[1] =~ /^[0-9]+$/
		  and $elements[2] eq "$protocol"
	) or next;

	if ( defined $elements[3] ) {
		print "$elements[1]\t$elements[0]\t$elements[3]\n";
	}
	else {
		print "$elements[1]\t$elements[0]\t\n";
	}
}
